import java.util.Scanner;

public class Atm {
    private double withdrawMaximumCash;
    private double currentSumInAtm;
    private double maxBalanceInAtm;
    private int transactionCounter;

    public Atm(double withdrawMaximumCash, double maxBalanceInAtm) {
        this.withdrawMaximumCash = withdrawMaximumCash;
        this.maxBalanceInAtm = maxBalanceInAtm;
    }

    public double putOnMoney(double incomeMoney) {
        double change = 0;
        if (getCurrentSumInAtm() + incomeMoney >= getMaxBalanceInAtm()) {
            change = getCurrentSumInAtm() + incomeMoney - getMaxBalanceInAtm();
            System.out.println("Сумма превышает лимит АТМ, заберить сдачу:  " + change);
            transactionCounter();
            return change;
        } else {
            System.out.println("Принято:  " + incomeMoney);
        }
        transactionCounter();
        return 0;
    }

    public double outcush(double cush) {
        if (getWithdrawMaximumCash() < cush) {
            System.out.println("Не коректная сумма\n Максимальная сумма снятия  " + getWithdrawMaximumCash());
        } else {
            System.out.println(" Выдача " + cush);
        }
        transactionCounter();
        return cush;
    }

    public void transactionCounter() {
        transactionCounter++;
    }


    public double getWithdrawMaximumCash() {
        return withdrawMaximumCash;
    }

    public double getCurrentSumInAtm() {
        return currentSumInAtm;
    }

    public double getMaxBalanceInAtm() {
        return maxBalanceInAtm;
    }

    public int getTransactionCounter() {
        return transactionCounter;
    }
}