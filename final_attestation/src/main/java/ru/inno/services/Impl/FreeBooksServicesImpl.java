package ru.inno.services.Impl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.models.FreeBook;
import ru.inno.repositories.FreeBooksRepository;
import ru.inno.services.FreeBooksServices;

import java.util.List;

@Service
@RequiredArgsConstructor
@Data
public class FreeBooksServicesImpl implements FreeBooksServices {
    private final FreeBooksRepository freeBooksRepository;

    @Override
    public List<FreeBook> getAllFreeBooks() {
        return freeBooksRepository.findAllByStateNot(FreeBook.State.DELETED);
    }

    @Override
    public FreeBook getFreeBook(Long id) {
        return freeBooksRepository.findById(id).orElseThrow();
    }

    @Override
    public void addFreeBook(FreeBook freeBook) {
        FreeBook newFreeBook = FreeBook.builder()
                .title(freeBook.getTitle())
                .description(freeBook.getDescription())
                .state(FreeBook.State.NOT_CONFIRMED)
                .build();
        freeBooksRepository.save(newFreeBook);
    }

    @Override
    public void updateFreeBook(Long freeBookId, FreeBook updateData) {
        FreeBook freeBookForUpdate = freeBooksRepository.findById(freeBookId).orElseThrow();
        freeBookForUpdate.setTitle(updateData.getTitle());
        freeBookForUpdate.setDescription(updateData.getDescription());
        freeBooksRepository.save(freeBookForUpdate);
    }

    @Override
    public void deleteFreeBook(Long freeBookId) {
        FreeBook freeBookForDelete = freeBooksRepository.findById(freeBookId).orElseThrow();
        freeBookForDelete.setState(FreeBook.State.DELETED);
        freeBooksRepository.save(freeBookForDelete);
    }
}
