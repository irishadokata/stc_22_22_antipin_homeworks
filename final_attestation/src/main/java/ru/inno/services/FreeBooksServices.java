package ru.inno.services;

import ru.inno.models.FreeBook;

import java.util.List;

public interface FreeBooksServices {

    List<FreeBook> getAllFreeBooks();

    FreeBook getFreeBook(Long id);
    void addFreeBook(FreeBook freeBook);

    void updateFreeBook(Long freeBookId, FreeBook freeBook);

    void deleteFreeBook(Long freeBookId);
}
