package ru.inno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.models.Reader;

import java.util.List;
import java.util.Optional;

public interface ReadersRepository extends JpaRepository<Reader, Long> {
    List<Reader> findAllByStateNot(Reader.State deleted);

Optional<Reader> findByEmail(String email);
}
