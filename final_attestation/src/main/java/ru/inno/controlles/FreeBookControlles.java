package ru.inno.controlles;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.models.FreeBook;
import ru.inno.models.Reader;
import ru.inno.repositories.FreeBooksRepository;
import ru.inno.security.details.CastomReaderDetails;
import ru.inno.services.FreeBooksServices;

@RequiredArgsConstructor
@Controller
public class FreeBookControlles {
    private final FreeBooksServices freeBooksServices;

    @GetMapping("/freebooks")
    public String getFreeBooksPage(@AuthenticationPrincipal CastomReaderDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getReader().getRole());
        model.addAttribute("freebooks", freeBooksServices.getAllFreeBooks());
        return "FreeBooks";
    }
    @PostMapping("/freebooks")
    public String addFreeBook(FreeBook freeBook) {
        freeBooksServices.addFreeBook(freeBook);
        return "redirect:/freebooks";
    }
    @GetMapping("/freebooks/{freebook-id}")
    public String getFreeBookPage(@PathVariable("freebook-id") Long id, Model model) {
        model.addAttribute("freebook", freeBooksServices.getFreeBook(id));
        return "FreeBook";
    }

    @PostMapping("/freebooks/{freebook-id}/update")
    public String updateFreeBook(@PathVariable("freebook-id") Long freeBookId,FreeBook freeBook) {
        freeBooksServices.updateFreeBook(freeBookId, freeBook);
        return "redirect:/freebooks/" + freeBookId;
    }

    @GetMapping("/freebooks/{freebook-id}/delete")
    public String updateFreeBook(@PathVariable("freebook-id") Long freeBookId) {
        freeBooksServices.deleteFreeBook(freeBookId);
        return "redirect:/freebooks";
    }
}
