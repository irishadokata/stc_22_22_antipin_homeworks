package ru.inno.controlles;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.inno.models.Reader;
import ru.inno.security.details.CastomReaderDetails;

@RequiredArgsConstructor
@Controller
public class ProfileController {

    @GetMapping("/")
    public String getRoot() {
        return "redirect:/profile";
    }

    @GetMapping("/profile")
    public String getProfile(@AuthenticationPrincipal CastomReaderDetails userDetails,
                             Model model) {
        Reader reader = userDetails.getReader();
        model.addAttribute("reader", reader);
        return "profile";
    }
}
