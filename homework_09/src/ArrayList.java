

public class ArrayList<T> implements List<T> {
    private final static int DEFAULT_ARRAY_SIZE = 10;
    private T[] elements;
    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {
        int currentLength = elements.length;
        int newLength = currentLength + currentLength / 2;
        T[] newElements = (T[]) new Object[newLength];
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public void remove(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                removeAt(i);
            }
        }
    }

    @Override
    public void removeAt(int index) {
        for (int i = index; i < count - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[count - 1] = null;
        count--;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    static void printCollection(List<Integer> integerArrayList) {
        for (int i = 0; i < integerArrayList.size(); i++) {
            System.out.print(integerArrayList.get(i) + " ");
        }
    }
}
